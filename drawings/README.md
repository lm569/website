# Diagrams and drawings

This directory contains source documents for diagrams and drawings within the
site. They are the following formats:

* `.drawio` - drawings created via https://app.diagrams.net/.
