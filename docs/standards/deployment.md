# Deployment and configuration management

!!! abstract

    Service management usually involves a *deployment* or *configuration
    management* step where updates to the service are made "live" to users. This
    page discusses relavent standards from the Technical Design Authority (TDA)
    related to the deployment or configuration of services.

    This page uses technical terminology and is intended for those deploying or
    developing services.

Deployment and configuration of services should be:

* **Automated** Deployments should not involve manual changes made to
    configuration or manual updates to servers.
* **Repeatable** It should be possible to spin up a parallel environment at
    relatively short notice. This is useful both for testing and disaster
    recovery. Avoid making "production" an environment which cannot be
    re-created from scratch if necessary.
* **Documented** Configuration of a service away from "defaults" should be
    explained and justified in accompanying documentation. In particular, any
    configuration steps which are *not* automated should be accompanied by a
    justification as to why they are required to be so.
* **Idempotent** Automated deployment processes should make no unnecessary
    changes. Re-deploying the same release to an environment should result in no
    change.
* **Versioned** Configuration and documentation related to deployment must be
    kept in a version control system. This naturally encourages an
    [infrastructure as
    code](https://en.wikipedia.org/wiki/Infrastructure_as_code) model. It is
    recommended that the [University Developers'
    Hub](https://gitlab.developers.cam.ac.uk) be used to version deployment
    configuration.

The Technical Design Authority (TDA) recognises that technical requirements
surrounding deployment can have a [long
tail](https://en.wikipedia.org/wiki/Long_tail) with some services requiring
vendor-specific tooling. This page provides generic guidance for cases where
it is technically feasible to choose one's deployment and configuration tooling.

## Recommended tools

This section discusses recommended tools for various scenarios.

!!! note

    If you feel there is a scenario which is not included, the TDA would
    welcome examples of best practice within UIS which could be added to this
    page.

### Virtual machines

The TDA recommends the use of [Ansible](https://www.ansible.com/) for the
provisioning and on-going configuration management of virtual machines providing
services within UIS. It can be run under Linux, Mac OS X and
[WSL2](https://docs.microsoft.com/en-us/windows/wsl/) on Windows.

Ansible has the following desirable properties:

  * It can configure both Unix-like systems and Microsoft Windows. It includes
    its own declarative language to describe system configuration which can be
    versioned.
  * It makes minimal changes; if the target system is already
    configured it makes no changes.
  * It is well-established within UIS and expertise exists in many groups.

Some common things to check with your Ansible configuration:

  * Running `ansible-playbook` with `--check` succeeds.
  * Running `ansible-playbook` with `--check` reports no changes if the service
    is up to date.
  * Ansible tasks should not report unnecessary changes in either check-mode or
    when actually run.
  * Removing a task from a playbook does *not* automatically "undo" it on the
    target server. You should replace a positive constraint with a negative one.
    For example: do not remove a "user abc123 must exist" constraint without
    adding a "user abc123 must *not* exist" one to replace it.
  * Your playbook is "lint-clean" when checked with a linting tool. The TDA does
    not mandate a particular linter but
    [ansible-lint](https://ansible-lint.readthedocs.io/en/latest/) is used
    by some groups within UIS to keep their playbooks clean.

You should ensure that your playbooks remain clean by running a lint tool as
part of a Continuous Integration process.

### Cloud and on-premises infrastructure

The TDA recommends [Terraform](https://www.terraform.io/) for infrastructure
management. Terraform supports the following clouds:

* Amazon Web Services via a dedicated
  [provider](https://registry.terraform.io/providers/hashicorp/aws/latest) and
  [first-party
  modules](https://registry.terraform.io/namespaces/terraform-aws-modules).
* Azure via a dedicated
  [provider](https://registry.terraform.io/providers/hashicorp/azurerm/latest)
  and [first-party modules](https://registry.terraform.io/namespaces/aztfmod).
* Google Cloud via a dedicated
  [provider](https://registry.terraform.io/providers/hashicorp/google/latest)
  and [first-party
  modules](https://registry.terraform.io/namespaces/terraform-google-modules).
* OpenStack via a dedicated
  [provider](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest).
* Various VMWare products via dedicated
  [providers](https://registry.terraform.io/search/providers?namespace=vmware).

Terraform supports a great deal of additional infrastructure options by means of
multiple [providers](https://registry.terraform.io/browse/providers).

!!! example

    The API gateway service is managed using Terraform and a [terraform
    provider](https://github.com/zambien/terraform-provider-apigee/) for the API
    gateway product providing the service.

Terraform's configuration language is
[declarative](https://en.wikipedia.org/wiki/Declarative_programming) meaning that
one describes what the desired infrastructure should look like and terraform
makes the required changes to make it so. As such it is naturally *repeatable*,
*automated* and *idempotent*. Terraform configurations may be *versioned* and
*documented* in the [University Developers'
Hub](https://gitlab.developers.cam.ac.uk/).

The `terraform` command-line tool comes with an opinionated formatter and
syntax checker known as `terraform fmt`. The TDA strongly recommends that
`terraform fmt` be run as part of a Continuous Integration process.

As Terraform is an automated tool, it can be used as part of a Continuous
Deployment process to ensure that the `master` branch of a git repository
reflects the currently deployed infrastructure.
