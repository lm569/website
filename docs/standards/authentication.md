# Authentication

!!! abstract

    *Authentication* refers to the process by which a service ascertains the
    identity of the current user. *Authorisation* refers to the process by which
    a service determines if that user is able to perform an action.

    This page describes the Technical Design Authority (TDA) standards for
    *authentication* of "natural" users. It does not apply to service-to-service
    authentication, sometimes known as "service account" authentication.

    We use technical terminology below and this advice is intended for those
    delivering or developing services.

## Acceptable protocols

The Technical Design Authority (TDA) requires that new or updated services use
the following authentication protocols:

1. For interactive services, e.g. web applications, authenticate users by
   means of one of [OpenID Connect](https://openid.net/connect/),
   [OAuth2](https://tools.ietf.org/html/rfc6749),
   [SAML2](https://tools.ietf.org/html/rfc7522) or mechanisms offered by ADFS
   which do not involve the service directly handling passwords.
2. For non-interactive (e.g. shell console) services shall use one of Kerberos
   or LDAPS. For these use cases, prefer a "token" based system where tokens are
   obtained using one of the web authentication protocols above.

Only the protocols above may be used and this applies equally to services based
on a third-party product and services developed "in-house".

Interactive services must not "cheat" by simply front-ending on a
non-interactive service.  For example, a website must not ask for a username and
password and then check in the backend with an LDAPS server if the credentials
are correct. Instead, the website should use the SAML2 protocol and redirect the
user to the SAML2 Authentication portal.

Existing services should transition to an approved authentication mechanism as
soon as practical.

## Proliferation of sign in boxes

The TDA and UIS as a whole discourage users within the University from entering
their credentials as a "default option" when presented with a sign in box. This
is clear advice can be confused by services which provide their own sign in box
UI.

Services must *not* provide their own sign in box UI as the recommended method
of signing in. They must instead use the [Raven
service](https://docs.raven.cam.ac.uk/) for sign in. A sign in box UI may be
provided as a *secondary* means of sign in.

!!! example

    The install of GitLab in the [University Developers'
    Hub](https://gitlab.developers.cam.ac.uk/) encourages the use of Raven as a
    primary sign in. Once an account is created, users may associate their
    account with multiple other "social" identities or make use of a custom
    password. This is an example of supporting "secondary" means of
    authentication.

The Raven service is a member of the [UK Access Management
Federation](https://www.jisc.ac.uk/uk-federation) (UKAMF) meaning that services
which enable sign in of UKAMF via SAML 2.0 will also support Raven.

Raven currently offers two authentication protocols:
[SAML2](https://tools.ietf.org/html/rfc7522) and [OpenID
Connect](https://openid.net/connect/) (OIDC).
[OAuth2](https://tools.ietf.org/html/rfc6749) is a subset of OIDC.

If your service only supports Active Directory authentication or you have a
system that requires directly handling user passwords, you should use [Blue
AD](https://help.uis.cam.ac.uk/service/accounts-passwords/it-staff/blue-ad).
